#! /usr/bin/env python3
"""Script to test the specfunc Python module."""

# Python imports
import os
import sys
import time

# Module imports
import numpy as np
import scipy.special as ss
import mpmath as mp

# Local imports
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)
import src as sf


def mp_loghyp1f1(a, b, z):
    """Wrap around the mpmath.hyp1f1 function."""
    out = np.zeros_like(z, dtype=np.float64)
    for i in range(a.size):
        out[0, i] = np.array(mp.log(mp.hyp1f1(a[0, i], b[0, i], z[0, i])), dtype=np.float64)
    return out


def test_gammaln():
    """Test the gammaln function."""

    print("ln(gamma(x)):")
    gammaln_known = np.array([
        0, 0, 0.693147, 1.791759, 3.178054, 4.787492, 6.579251, 8.525161,
    ])
    x = np.arange(0, gammaln_known.size).reshape(1, -1) + 1
    glx = sf.gammaln(x)
    glxs = ss.loggamma(x)
    print("Input\tKnown\t\tspecfunc\tscipy\n")
    for i in range(x.size):
        print((
            f"{x[0, i]}:\t{gammaln_known[i]:.6f}\t"
            f"{glx[0, i]:.6f}\t{glxs[0, i]:.6f}"
        ))

    assert np.all(np.abs(glx - glxs) < 1e-9)

    x = np.arange(0, 1000).reshape(1, -1) + 1

    time_sf = time.time()
    glx = sf.gammaln(x)
    time_sf = time.time() - time_sf

    time_ss = time.time()
    glxs = ss.loggamma(x)
    time_ss = time.time() - time_ss

    assert np.all(np.abs(glx - glxs) < 1e-9)

    print(f"\nspecfunc:\t{time_sf:.6f} s")
    print(f"spicy:\t\t{time_ss:.6f} s")


def test_hyp1f1():
    """Tests for the hyp1f1 function"""

    print("\nhyp1f1(a, b, z):")
    a = np.array([1, 10, 10, 100, 12]).reshape(1, -1)
    b = np.array([1, 1, 10, 10, 14]).reshape(1, -1)
    z = np.array([1, 10, 10, 1, 0.4]).reshape(1, -1)
    hyp1f1_known = np.array([
        2.718282,
        8514625477,
        22026.47,
        2486.218,
        1.409877,
    ])
    hgfv = sf.hyp1f1(a, b, z)
    hgfvs = ss.hyp1f1(a, b, z)
    print("Input\t\tKnown\t\tspecfunc\tscipy\n")
    for i in range(a.size):
        print((
            f"({a[0, i]},{b[0, i]},{z[0, i]}):\t{hyp1f1_known[i]:.9}\t"
            f"{hgfv[0, i]:.9}\t{hgfvs[0, i]:.9}"
        ))

    a = np.zeros((1, 1000)) + 12
    b = np.zeros((1, 1000)) + 14
    z = np.linspace(1, 2, 1000).reshape(1, -1)

    time_sf = time.time()
    hgfv = sf.hyp1f1(a, b, z)
    time_sf = time.time() - time_sf

    time_ss = time.time()
    hgfvs = ss.hyp1f1(a, b, z)
    time_ss = time.time() - time_ss

    print(f"\nspecfunc:\t{time_sf:.6f} s")
    print(f"spicy:\t\t{time_ss:.6f} s")

    assert np.all(np.abs(hgfv - hgfvs) / hgfv < 1e-7)


def test_hyp1f1ln():
    """Tests for the hyp1f1ln function"""

    print("\nhyp1f1ln(a, b, z):")

    a = np.array([1, 10, 10, 100, 12]).reshape(1, -1)
    b = np.array([1, 1, 10, 10, 14]).reshape(1, -1)
    z = np.array([1, 10, 10, 1, 0.4]).reshape(1, -1)

    hyp1f1ln_known = np.array([
        1,
        22.86505,
        10,
        7.818518,
        0.3435025,
    ])
    lhgfv = sf.hyp1f1ln(a, b, z)
    lhgfvs = np.log(ss.hyp1f1(a, b, z))
    print("Input\t\tKnown\t\tspecfunc\tscipy\n")
    for i in range(a.size):
        print((
            f"({a[0, i]},{b[0, i]},{z[0, i]}):\t{hyp1f1ln_known[i]:.7f}\t"
            f"{lhgfv[0, i]:.7f}\t{lhgfvs[0, i]:.7f}"
        ))

    size = 100000
    a = np.zeros((1, size)) + 80
    b = np.zeros((1, size)) + 9
    z = np.linspace(1, 2, size).reshape(1, -1)

    time_sf = time.time()
    hgfv = sf.hyp1f1ln(a, b, z)
    time_sf = time.time() - time_sf

    time_ss = time.time()
    hgfvs = np.log(ss.hyp1f1(a, b, z))
    time_ss = time.time() - time_ss

    time_sm = time.time()
    hgfvm = mp_loghyp1f1(a, b, z)
    time_sm = time.time() - time_sm

    print(f"\nspecfunc:\t{time_sf:.6f} s")
    print(f"spicy:\t\t{time_ss:.6f} s")
    print(f"mpmath:\t\t{time_sm:.6f} s")

    assert np.all(np.abs(hgfv - hgfvm) / hgfvm < 1e-7)

    print(f"Scipy max abs percentage error: {np.max(np.abs(hgfv - hgfvs) / hgfv)}")


if __name__ == "__main__":
    test_gammaln()
    test_hyp1f1()
    test_hyp1f1ln()
