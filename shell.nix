{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  NIX_SHELL_PRESERVE_PROMPT=1;
  buildInputs = [
    pkgs.python311
    pkgs.python311Packages.poetry-core
    pkgs.stdenv.cc.cc.lib
    pkgs.libz
  ];
  shellHook = ''
    export LD_LIBRARY_PATH=${pkgs.lib.makeLibraryPath [
      pkgs.stdenv.cc.cc
      pkgs.libz
    ]}
    export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/lib/gcc/x86_64-pc-linux-gnu/13/"
  '';
}
